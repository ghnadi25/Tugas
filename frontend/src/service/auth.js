import { axiosBackend } from './index'
// import router from '@/router'


export default {
    login,
    getAuth,
    logout,
    isLogedIn,
}

function login (username, password) {
    const field = {
        username,
        password,
    }

  return axiosBackend.post('login', field).then(data => {
        if (data.status === 'success') {
            const userAuth = {
                userId: data.data.userId,
                profileId: data.data.profileId,
                username: data.data.username,
                rule: data.data.rule,
                token: data.token,
            }

            isLogedIn(userAuth)
            return data
        } else {
            return data
        }
    },
    )
}


function isLogedIn (data) {
    localStorage.setItem('user-auth', JSON.stringify(data))
}

function getAuth () {
    return JSON.parse(localStorage.getItem('user-auth'))
}

function logout () {
    localStorage.removeItem('user-auth')
}
