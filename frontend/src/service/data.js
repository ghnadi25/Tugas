import { axiosBackend, authHeader } from './'
// import store from '../stores/index'



export default {
    findAll,
    create,
    destroy,
    edit,
    findOne,
}

async function findAll (path, params) {
   try {
       return await axiosBackend({
           method: 'GET',
           headers: authHeader(),
           url: path,
           params,
       })
   } catch (err) {
        return { error: err.message }
   }
}

async function create (path, data) {
    try {
        return await axiosBackend({
            method: 'POST',
            headers: authHeader(),
            url: path,
            data,
        })
    } catch (err) {
        return { error: err.message }
    }
}

async function destroy (path, params) {
    try {
        return await axiosBackend({
            method: 'DELETE',
            headers: authHeader(),
            url: path,
            params,
        })
    } catch (err) {
        return { error: err.message }
    }
}

async function edit (path, data, params) {
    try {
        return await axiosBackend({
            method: 'PUT',
            headers: authHeader(),
            url: path,
            data,
            params,
        })
    } catch (err) {
        return { error: err.message }
    }
}

async function findOne (path) {
    try {
        return await axiosBackend({
            method: 'GET',
            headers: authHeader(),
            url: path,
        })
    } catch (err) {
        return { error: err.message }
    }
}
