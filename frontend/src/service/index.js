import axios from 'axios'

import { config } from '@/config'
import authService from './auth'


export const axiosBackend = axios.create({
    baseURL: config.backendUrl,
    timeout: 30000,
})

export function authHeader () {
    const getToken = authService.getAuth()
    return getToken.token ? { Authorization: getToken.token } : {}
}

axiosBackend.interceptors.response.use(
    response => Promise.resolve(response.data),
    error => {
        return error.response.data
    },
)
