import Vue from 'vue'
import Router from 'vue-router'
import store from './stores/index'
// import vm from './main'

Vue.use(Router)

export const router = new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      component: () => import('@/views/dashboard/pages/LoginPage'),
      meta: {
        role: 'public',
      },
    },
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        {
          name: 'Home',
          path: '',
          meta: {
            role: 'public',
          },
          component: () => import('@/views/dashboard/Home'),
        },
        // Dashboard
        {
          name: 'Dashboard',
          path: '/dbadmin',
          meta: {
            role: 'admin',
          },
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        {
          name: 'User Profile',
          path: '/user/admin',
          meta: {
            role: 'admin',
          },
          component: () => import('@/views/dashboard/profile/UserProfile'),
        },
        {
          name: 'User Profile',
          path: '/user/petani',
          meta: {
            role: 'petani',
          },
          component: () => import('@/views/dashboard/profile/UserProfile'),
        },
        {
          name: 'Devices',
          path: '/devices',
          meta: {
            role: 'admin',
          },
          component: () => import('@/views/dashboard/devices/DevicesIndex'),
        },
        {
          name: 'deviceLogs',
          path: '/device/:id/log',
          meta: {
            role: 'admin',
          },
          component: () => import('@/views/dashboard/devices/DataLogs'),
        },
        {
          name: 'petani',
          path: '/petani',
          meta: {
            role: 'admin',
          },
          component: () => import('@/views/dashboard/petani/PetaniIndex'),
        },
        {
          name: 'Petani device',
          path: '/petani-devices/:id',
          meta: {
            role: 'petani',
          },
          component: () => import('@/views/dashboard/pagePetani/Device'),
        },
        {
          name: 'Dashboard Petani',
          path: '/petani-dashboard',
          meta: {
            role: 'petani',
          },
          component: () => import('@/views/dashboard/pagePetani/Dashboard'),
        },
        {
          name: 'Pengaturan jadwal',
          path: '/petani-device/config',
          meta: {
            role: 'admin',
          },
          component: () => import('@/views/dashboard/schedule/Schedule'),
        },
        // Upgrade
        {
          name: 'Upgrade',
          path: 'upgrade',
          meta: {
            role: 'public',
          },
          component: () => import('@/views/dashboard/Upgrade'),
        },
      ],
    },
  ],
})

router.beforeEach((to, from, next) => {
  const role = to.matched.some(r => (r.meta.role === store.getters['auth/rule']) || r.meta.role === 'public')
  if (!role) {
    store.dispatch('auth/logout')
    // console.log(store.getters['auth/role'])
  } else {
    next()
  }
})


export default router
