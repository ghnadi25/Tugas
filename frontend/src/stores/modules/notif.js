export const notif = ({
  namespaced: true,
  state: {
    snackbar: false,
    text: ' ',
    color: ' ',
  },
  mutations: {
    SET_SUCCESS (state, text) {
      state.snackbar = true
      state.text = text
      state.color = 'info'
    },
    SET_ERROR (state, text) {
      state.snackbar = true
      state.text = text
      state.color = 'error'
    },

    SET_SNACKBAR (state, val) {
      state.snackbar = val
    },
  },
  actions: {
    success ({ commit }, { text }) {
        return commit('SET_SUCCESS', text)
    },
    error ({ commit }, { text }) {
      return commit('SET_ERROR', text)
    },
    snackbar ({ commit }, { val }) {
      return commit('SET_SNACKBAR', val)
    },
  },
  getters: {
    snackbar: state => state.snackbar,
  },
})
