import dataService from '../../service/data'
import store from '../index'

export const data = ({
    namespaced: true,
    state: {
        count: 0,
        limit: 10,
        data: [],
        loading: false,
        filter: {},
    },
    mutations: {
        SET_DATA (state, data) {
            state.data = data.data
            state.count = data.count
        },
        SET_FILTER (state, filter) {
            state.filter = filter
        },
        SET_LIMIT (state, limit) {
            state.limit = limit
        },
    },
    actions: {
        get ({ commit, state }, { path, filter }) {
            if (filter) {
                commit('SET_FILTER', filter)
            }
            return dataService.findAll(path, state.filter).then(res => {
                if (res.status === 'success') {
                    commit('SET_DATA', res)
                } else {
                    store.dispatch('notif/error', { text: res.message })
                    store.dispatch('auth/logout')
                }
            })
        },
        post ({ commit }, { path, data }) {
            return dataService.create(path, data).then(res => {
                if (res.error) {
                    store.dispatch('notif/error', { text: res.error })
                    store.dispatch('auth/logout')
                } else {
                    store.dispatch('notif/success', { text: res.message })
                    return res
                }
            })
        },
        destroy ({ commit }, { path, filter }) {
            return dataService.destroy(path, filter).then(res => {
                if (res.error) {
                    store.dispatch('notif/error', { text: res.error })
                    store.dispatch('auth/logout')
                } else {
                    store.dispatch('notif/success', { text: res.message })
                    return res
                }
            })
        },
        edit ({ commit }, { path, data, filter }) {
            return dataService.edit(path, data, filter).then(res => {
                if (res.error) {
                    store.dispatch('notif/error', { text: res.error })
                    store.dispatch('auth/logout')
                } else {
                    store.dispatch('notif/success', { text: res.message })
                    return res
                }
            })
        },
        resetFilter ({ commit }) {
            const filter = {
                limit: 10,
                pages: 1,
            }

            commit('SET_FILTER', filter)
        },

        setFilter ({ commit }, { filter }) {
            commit('SET_FILTER', filter)
        },

        setLimit ({ commit }, { limit }) {
            commit('SET_LIMIT', limit)
        },
    },
    getters: {
        data: state => {
            return state.data
        },
        pages: state => {
            const pages = Number(state.count) / Number(state.filter.limit)
            return pages >= 1 ? pages : 1
        },
    },
})
