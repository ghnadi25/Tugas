export const chart = ({
   namespaced: true,
   state: {
      data: {},
      onFilter: false,
      filter: {},
      options: {},
   },
   mutations: {
      'SOCKET_GET_DATA_SENSOR' (state, server) {
         state.data = server
      },
   },
   action: {
      initC3Chart ({ commit }, { options }) {

      },
   },
   getters: {
      time_chart: state => {
         return state.data.time ? state.data.time : {}
      },
      air_temprature: state => {
         return state.data.air_temprature ? state.data.air_temprature : []
      },
      
      air_humidity: state => {
        return state.data.air_humidity ? state.data.air_humidity : []
      },
      soil_moisture: state => {
         return state.data.soil_moisture ? state.data.soil_moisture : []
      },
   },
})
