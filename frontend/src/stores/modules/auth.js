import authService from '@/service/auth'
import router from '@/router'
import store from '../index'

const user = authService.getAuth()
const initialState = user ? { loggedIn: true, user } : { loggedIn: false, user: null }
export const auth = ({
   namespaced: true,
   state: initialState,
   mutations: {
      LOGIN_SUCCESS (state, user) {
         state.user = user
         state.loggedIn = true
      },
      LOGIN_FAILED (state) {
         state.user = null
         state.loggedIn = false
      },
      LOGOUT (state) {
         state.user = null
         state.loggedIn = false
      },
   },
   actions: {
      login ({ commit }, { data }) {
         authService.login(data.username, data.password).then(res => {
            if (res.status === 'success') {
               // vm.$socket.connect()
               commit('LOGIN_SUCCESS', res.data)
               store.dispatch('notif/success', { text: res.message }, { root: true })
               router.push('/')
            } else {
               commit('LOGIN_FAILED')
               store.dispatch('notif/error', { text: res.message }, { root: true })
            }
            },
         )
      },
      logout ({ commit }) {
         commit('LOGOUT')
         authService.logout()
         router.push('/login')
      },
      SOCKET_login ({ commit }, { data }) {
         this._vm.$socket.emit('user-login', { data })
      },
   },
   getters: {
      rule: state => state.user ? state.user.rule : '',
      token: state => state.user ? state.user.token : '',
      username: state => state.user ? state.user.username : '',
      user: state => state.user ? state.user : null,
   },
})
