export const app = ({
  namespaced: true,
  state: {
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    barImage: 'https://demos.creative-tim.com/material-dashboard/assets/img/sidebar-1.jpg',
    drawer: true,
  },
  mutations: {
    SET_BAR_IMAGE (state, payload) {
      state.barImage = payload
    },
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
    'SOCKET_SET_DRAWER' (state, server) {
      state.drawer = server.data
    },
  },
  actions: {
    drawer ({ commit }, { val }) {
      return commit('SET_DRAWER', val)
    },

    
  },
})
