import { Vue } from '@/vueglobal'
import Vuex from 'vuex'


import { app } from './modules/app'
import { chart } from './modules/chart'
import { auth } from './modules/auth'
import { notif } from './modules/notif'
import { data } from './modules/data'

Vue.use(Vuex)

export default new Vuex.Store({
  
  modules: {
    app,
    chart,
    auth,
    notif,
    data,
  },
})
