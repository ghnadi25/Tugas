import Vue from 'vue'
import store from './stores'
import App from './App.vue'
import VueSocketIO from 'vue-socket.io'
import SocketIO from "socket.io-client"

export default Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://localhost:5500',
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    },
    options: { path: "/my-app/" } //Optional options
}))
