// =========================================================
// * Vuetify Material Dashboard - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import { Vue } from '@/vueglobal'
import App from './App.vue'
import router from './router'
import store from './stores'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import VueSocketIO from 'vue-socket.io'
import 'c3/c3.min.css'
import { config } from './config'

Vue.config.productionTip = false
Vue.prototype.$eventbus = new Vue()
Vue.prototype.$store = store


Vue.use(new VueSocketIO({
  debug: true,
  connection: config.backendUrl,
  options: { autoConnect: false },
  vuex: {
      store,
      actionPrefix: 'SOCKET_',
      mutationPrefix: 'SOCKET_',
  },
}))


export default new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App),
}).$mount('#app')
