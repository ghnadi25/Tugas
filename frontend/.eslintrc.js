module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: 'vuetify',
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "no-multiple-empty-lines": [2, {"max": 99999, "maxEOF": 0}],
    'no-trailing-spaces': ["error", { "skipBlankLines": true }]
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
}
