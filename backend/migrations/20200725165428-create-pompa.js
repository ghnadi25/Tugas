'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('pompas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      mode: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        set: function(value) {
          if (value === 'true') value = true;
          if (value === 'false') value = false;
          this.setDataValue('hidden', value);
        }
      },
      water_pump: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        set: function(value) {
          if (value === 'true') value = true;
          if (value === 'false') value = false;
          this.setDataValue('hidden', value);
        }
      },
      fertilizer_pump: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        set: function(value) {
          if (value === 'true') value = true;
          if (value === 'false') value = false;
          this.setDataValue('hidden', value);
        }
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('pompas');
  }
};