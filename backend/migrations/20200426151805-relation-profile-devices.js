'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'devices',
      'profile_id',
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'profile',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    )
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('devices', 'profile_id')
  }
};
