'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return Promise.all(
      [
       queryInterface.addColumn('devices', 'usia_tanam', {
           type: Sequelize.INTEGER,
           allowNull: false,
           defaultValue: 1
       }),
       queryInterface.addColumn('devices', 'waktu_tanam', {
           type: Sequelize.DATE,
           allowNull: false,
           defaultValue: Sequelize.fn('now')
       })
     ]
    )
    
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all(
      [
        queryInterface.removeColumn('devices', 'usia_tanam'),
        queryInterface.removeColumn('devices', 'waktu_tanam')
      ] 
    )
  }
};
