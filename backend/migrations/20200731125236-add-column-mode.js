'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'devices',
      'mode', {
        type: Sequelize.INTEGER,
        defaultValue: 2
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('devices', 'mode')
  }
};
