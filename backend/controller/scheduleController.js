const model = require('../models')
const  Op = require('sequelize').Op

async function getAll(req, res){
    let query = req.query
    let options = {
        order: [
            ['id', 'DESC']
        ],
    }

    if (query.s) {
        let s = query.s
        options.where = {
            [Op.or]: {
                usia: { [Op.like]: `%${s}%`},
                volume_air: {[Op.like]: Number(`%${s}%`)}
            }
        }
    }

    if(query.limit) {
        options.limit = Number(query.limit)
    }

    if(query.pages) {
        options.offset = Number(query.pages) > 1 ? Number(query.pages) : 0
    }


    try{

        let data = await model.schedules.findAll(options)

        let message = data.length >= 0 ? "Success" : "data empty" 

        return res.status(200).json({
            status: 'success',
            message,
            count: data.length,
            data,
        })
    } catch (err) {
        return res.json({
            message: err.message,
            count: null,
            data: []

        })

    }
}

async function createSchedule (req, res) {
    let {usia , volume_air, volume_pupuk} = req.body

    if(!usia && !volume_air && !volume_pupuk){
        return res.status(401).json({
            status: 'failed',
            message: 'field must be not null',
            data: [],
        })
    }

    try {

        await model.schedules.create({usia, volume_air, volume_pupuk})
        return res.status(201).json({
            status: "success",
            message: "Penjadwalan berhasil dibuat",
            data: [],
        })

    } catch (err) {
        return res.json({
            message: err.message,
            count: null,
            data: []

        })
    }
}

async function update (req, res) {
    let query = req.query
    let {usia , volume_air, volume_pupuk}  = req.body

    if(!usia && !volume_air && !volume_pupuk){
        return res.status(401).json({
            status: 'failed',
            message: 'field must be not null',
            data: [],
        })
    }

    if(!query.id) {
        return res.status(401).json({
            status: 'failed',
            message: 'url not valid',
            data: [],
        })
    }

    try {
        await model.schedules.update({usia, volume_air, volume_pupuk}, {
            where: {
                id: query.id
            }
        })
        return res.status(201).json({
            status: "success",
            message: "Penjadwalan berhasil diedit",
            data: [],
        })

    } catch (err) {
        return res.json({
            message: err.message,
            count: null,
            data: []

        })
    }
}

async function remove (req, res) {
    try {
        const query = req.query

        await model.schedules.destroy({
            where: {
                id: query.id
            }
        })

        return res.status(200).json({
            status: "Success",
            message: "Penjadwalan berhasil dihapus",
            data: [],
        })

    } catch (err) {
        return res.status(500).json({
            status: "failed",
            message: err.message,
            data: [],
        })
    }
}

module.exports = {
    getAll,
    createSchedule,
    update,
    remove,
}