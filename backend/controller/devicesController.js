const model = require('../models')
const Op = require('sequelize').Op
const uuid = require('uuid')
var mqtt = require('mqtt')
var cronHandler = require('../service/cronHandler')

async function getAll(req, res){
    let query = req.query
    let options = {
        order: [
            ['id', 'DESC']
        ],
        include: [{
            model: model.profile,
            as: 'profile'
        }],
    }

    if (query.s) {
        options.where = {
            [Op.or]: {
                lable: { [Op.like]: `%${query.s}%`},
                $nama_lengkap$: {[Op.like]: `%${query.s}%`}
            }
        }
    }

    if(query.limit) {
        options.limit = Number(query.limit)
    }

    if(query.pages) {
        options.offset = Number(query.pages) > 1 ? Number(query.pages) : 0
    }


    try{

        let data = await model.devices.findAll(options)

        let message = data.length >= 0 ? "Success" : "data empty" 

        return res.status(200).json({
            status: 'success',
            message,
            count: data.length,
            data,
        })
    } catch (err) {
        return res.json({
            message: err.message,
            count: null,
            data: []

        })

    }
}

async function create (req, res) {
    
    try {
        const {
            lable,
            profile_id
        } = req.body
        
        await model.devices.create({
            id: uuid.v4(),
            lable,
            profile_id
        })

        return res.status(201).json({
            status: "success",
            message: "Device created",
            data: [],
        })
       
    } catch (err) {
        return res.status(500).json({
            status: "failed",
            message: err.message,
            data: [],
        })
    }
}

async function remove (req, res) {
    try {
        const query = req.query

        await model.devices.destroy({
            where: {
                id: query.id
            }
        })

        return res.status(200).json({
            status: "Success",
            message: "Devices Deleted",
            data: [],
        })

    } catch (err) {
        return res.status(500).json({
            status: "failed",
            message: err.message,
            data: [],
        })
    }
}

async function edit (req, res) {
    try {
        const query = req.query
        const { lable, profile_id } = req.body

        await model.devices.update({
            lable,
            profile_id,
        },
        { 
            where: {
                id: query.id
            }
        })

        return res.status(200).json({
            status: "Success",
            message: "Devices Updated",
            data: [],
        })

    } catch (err) {
        return res.status(500).json({
            status: "failed",
            message: err.message,
            data: [],
        })
    }
}

async function getDevicesLogById(req, res) {
    let query = req.query
    let id = req.params.id

    if(!id){
        return res.status(401).json({
            status: 'failed',
            message: 'Url not valid',
            data: [],
        })
    }

    let options = {
        where: {
            id : id
        },
        include:[{
            model: model.DeviceLogs,
            as: 'deviceLogs',
        }],
        order: [
            // ['time', 'asc']
         ],
    }

    let countFilter = {
        where: {device_id : id}
    }



    if (query.limit) {
        options.include.forEach(key => {
            key.limit = Number(query.limit)
        })
    }else {
        options.include.forEach(key => {
            key.limit = 10
        })

    }

    if (query.pages) {
        options.include.forEach(key => {
            key.offset = Number(query.pages) > 1 ? (Number(query.pages) *  Number(query.limit)) - Number(query.limit) : 0
        })
    }


    if ( query.s ) {
        options.include.forEach(key => {
            key.where = {
                time: {
                    [Op.like]: `%${query.s}%`
                }
            }
        })

        countFilter.where = {
            [Op.and]: [ {device_id : id}, {
                time: {
                    [Op.like]: `%${query.s}%`
                }
            }]
        }
    }

    
    try {

        let data = await model.devices.findAll(options)
        count = await model.DeviceLogs.count(countFilter)
        let message = data.length >= 0 ? "Success" : "data empty"
       
        return res.status(200).json({
            status: 'success',
            message,
            count,
            data,
        })

    } catch (err) {
        return res.status(500).json({
            status: 'failed',
            message: err.message,
            data: [],
        })
    }
}

async function deviceListByPetaniId (req, res) {

    let petaniId = req.params.id

    let options = {
        where: {
            id: petaniId
        },
        include: [{
            model: model.devices,
            as: 'deviceList'
        }]
    }


    let dataResponse = await model.profile.findAll(options)
    let count = dataResponse.length
    let message = count >= 0 ? "Success" : "data empty"

   return res.status(200).json({
        status: 'success',
        message,
        count,
        data: dataResponse
    })

}

async function getPompaStatus (req, res) {
    let id = req.params.id

    if (!id) {
        return res.status(401).json({
            status: 'failed',
            message: 'Url not valid',
            data: [],
        })
    }

    try {
        let data = await model.pompa.findOne({where: {device_id: id}})

        return res.status(200).json({
            status: 'success',
            data
        })
        

    } catch (err) {
        return res.status(500).json({
            status: 'failed',
            message: err.message,
            data: [],
        })
    }
}

async function waterPumpControll(req, res) {
    let id = req.params.id
    let { waterValue, fertilizerValue} = req.body
    if(!id) {
        return res.status(401).json({
            status: 'failed',
            message: 'Url not valid',
            data: [],
        })
    }

    let checkDeviceId = await model.devices.findByPk(id)
    if(checkDeviceId) {

       let client = mqtt.connect(process.env.MQTT_HOST, { username: "admin", password: "admin" })
       client.publish('inTopic', `${id};0;${waterValue};${fertilizerValue};`)
    }

    return res.status(201).json({
        status: 'success',
        message: '',
        data: checkDeviceId
    })

}

async function scheduleControll (req, res) {
    let {id, value} = req.params
    console.log('test')

    if(!id || !value) {
        return res.status(401).json({
            status: 'failed',
            message: 'Url not valid',
            data: [],
        })
    }

    let data = value == 1 ? 1 : 2
    try {
        await model.devices.update(
            {mode: data},
            {where: {id}}
        )
         
         return res.status(201).json({
             status: 'success',
             message: '',
             data:[]
         }) 
    } catch (err) {
        return res.status(500).json({
            status: 'failed',
            message: err.message,
            data: [],
        })
    }




}


module.exports = {
    getAll,
    create,
    remove,
    edit,
    getDevicesLogById,
    deviceListByPetaniId,
    getPompaStatus,
    waterPumpControll,
    scheduleControll,
}