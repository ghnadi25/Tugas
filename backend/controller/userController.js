const model = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
response = require('../service/response');
const Op = require('sequelize').Op
const moment = require('moment')

async function create(req, res) {
    try{
        const {username, password, email} = req.body;
        const hasPassword = bcrypt.hashSync(password, 10);
        const user = await model.users.create({
            username,
            email,
            password : hasPassword,
        })

        if(user){
            res.status(201).json({
                status: 'success',
                message : 'User Created',
                data : []
            })
        }
    }
    catch(err){
        res.status(500).json(response.error(err.message));
    }
}


async function login(req, res){
    try {
        const {username, password} = req.body;

        const user = await model
                            .users
                            .findOne({ 
                                 include : 'rules',
                                 where : 
                                    {[Op.or]: [{'username' : username}, {'email': username}]}
                             });
        if(!user){
            throw res.status(401).json({
                status: 'failed',
                message: 'User not found',
                data: []
            });
        }

        const getConfig = await model.appConfigs.findOne({
            where: {
                config_name: 'token_expired'
            }
        })
        
        if(bcrypt.compareSync(password, user.password)){

           const token = jwt.sign({username : user.username, rule : user.rules.rule_name}, process.env.JWT_KEY, { expiresIn : `${Number(getConfig.value)}h`});

            const data = {
                userId : user.id,
                profileId: user.profile_id,
                username : user.username,
                email : user.email,
                rule : user.rules.rule_name,
            }

            res.status(200).json({
                status: 'success',
                message: 'Login Success',
                data,
                token,
            });


        }
        else{
           res.status(401).json({
               status: 'failed',
               message: 'wrong password',
               data: []
           });
        }
        
    }
    catch(err){
        res.status(500).json({
            status: 'failed',
            message: err.message,
            data: []
        });
    }
}

module.exports = {
    create,
    login,
}
