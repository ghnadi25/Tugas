const model = require('../models')
const Op = require('sequelize').Op
const bcrypt = require('bcrypt');

async function getAll(req, res){
    let query = req.query

    let options = {
        where: {
            $rule_name$: 'petani'
        },
        include: {
            model: model.users,
            as: 'users',
            attributes: [],
            include: {
                model: model.rules,
                as: 'rules',
            }
        },
        order: [
            ['id', 'ASC']
        ],
    }

    if(query.limit){
        options.limit = Number(query.limit)
    }

    if(query.pages){
        options.offset = Number(query.pages) > 1 ? Number(query.pages) : 0
    }

    if(query.s){
        options.where = {
            [Op.and]: [
                {$rule_name$: 'petani'},
                {
                    [Op.or]: [
                        {
                            $nama_lengkap$: 
                            {
                                [Op.like]: `%${query.s}%`
                            }
                        },
                        {
                            $alamat$: 
                            {
                                [Op.like]: `%${query.s}%`
                            },
                        },
                    ],
                },
            ],
        }
    }
    try{

        let data = await model.profile.findAll(options)

        let message = data.length >= 0 ? "Success" : "data empty" 

        return res.status(200).json({
            status: 'success',
            message,
            count: data.length,
            data,
        })
    } catch (err) {
        return res.status(500).json({
            message: err.message,
            count: null,
            data: []

        })

    }
}

async function createProfile (req, res) {
    let petani = await model.rules.findOne({
        where: {
            rule_name: 'petani'
        }
    })

    try{
        let {nama_lengkap, no_tlp, alamat, username, password, email} = req.body
        await model.profile.create({
            nama_lengkap,
            no_tlp,
            alamat
        }).then(async result => {
            await model.users.create({
                username,
                email,
                password: bcrypt.hashSync(password, 10),
                rule_id: petani.id,
                profile_id: result.id
            })
        })

        res.status(201).json({
            status: 'success',
            message: 'data successfuly added',
            data: [],
        })
    } catch (err) {
        res.status(500).json({
            status: 'failed',
            message: err.message,
            data: [],
        })
    }
}


async function remove (req, res) {
    const { id } = req.query

    if (!id) {
        res.status(401).json({
            status: 'failed',
            message: 'bad url',
            data: []
        })
    }
    try {
        
        await model.profile.destroy({
            where: {
                id,
            }
        })

        res.status(200).json({
            status: 'success',
            message: 'data deleted',
            data: [],
        })
    } catch (err) {
        res.status(500).json({
            status: 'failed',
            message: err.message,
            data: [],
        })
    }
}


async function getUserProfile(req, res){
    let id = req.params.id
    
    if (!id) {
        res.status(401).json({
            status: 'failed',
            message: 'bad url',
            data: []
        })
    }
    try {
        let user = await model.users.findAll({
            attributes: {
                exclude: ['password', 'profile_id', 'rule_id', 'id']
            },
            where: {
                profile_id: id
            },
            include: {
                model: model.profile,
                as: 'profile',
            }
        })

        console.log(user)
        res.status(200).json({
            status: 'success',
            message: 'success',
            data: user
        })

    } catch (err) {
        res.status(500).json({
            status: 'failed',
            message: err.message,
            data: []
        })
    }

}

async function updateProfile (req, res) {
    const { id } = req.query
    console.log(req.body)
    if (!id) {
        res.status(401).json({
            status: 'failed',
            message: 'empty parameter',
            data: []
        })
    }
    try {
        const { 
                nama_lengkap,
                no_tlp,
                alamat,
                username,
                password,
                email
            } = req.body
        
        await model.profile.update(
            {
                nama_lengkap,
                no_tlp,
                alamat,
            },
            {
                where: {
                    id
                }
            }
        ).then(async result => {
            const options = {
                username,
                email
            }

            if(password && password.length >= 1) {
                options.password = bcrypt.hashSync(password, 10)
            }

            await model.users.update(options, {
                where: {
                    profile_id: id
                }
            })
        })
       
        res.status(200).json({
            message: 'success',
            message: 'Data updated',
            data: []
        })
    } catch (err) {
        res.status(500).json({
            status: 'failed',
            message: err.message,
            data: [],
        })
    }
} 

module.exports = {
    getAll,
    createProfile,
    remove,
    getUserProfile,
    updateProfile,
}