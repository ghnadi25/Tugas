const model = require('../models/index')
const { param } = require('../routes')
const Op = require('sequelize').Op


async function storeSensorFromMqtt(data) {
   try {
      await model.DeviceLogs.create(data)
   } catch (err) {
      console.log(err)
   }
}


async function getData(params) {
   console.log(params)

   let options = {
      order: [
         ['id', 'DESC']
      ],
      include: [
         {
            model: model.devices,
            as: 'devices', 
         }
      ],
         limit: 20,
   }

   if(params.id){
      options.where = {
         device_id: params.id
      }
   }

   if(params.petaniId){
      console.log(params)
      let petani = await model.devices.findAll(
                        {
                           where: {profile_id: params.petaniId}
                        })
                        
      let deviceId = petani.map(m => m.id)

      options.where = {
         device_id: {
            [Op.in] : deviceId
         }
      }
   }

   try {

      let data = await model.DeviceLogs.findAll(options);

      
      let device = []
      data.filter(f => {
         if(!device.includes(f.devices.lable)){
            device.push(f.devices.lable)
         }
      })

      let dataXDevice = []
      let dataXAirHumidty = []
      let dataXSoilMoisture = []
      let dataXTime = []
      let keyData = {}
      device.forEach((el, index) => {
         let tempXairTemprature = data.filter(f => f.devices.lable == el).map(m => m.air_temprature_value)
         let tempXairHumidity = data.filter(f => f.devices.lable == el).map(m => m.air_humidity_value)
         let tempXSoilMoisture = data.filter(f => f.devices.lable == el).map(m => m.soil_moisture_value)
         let temTime =data.filter(f => f.devices.lable == el).map(m => m.time)

         dataXDevice.push([el, ...tempXairTemprature])
         dataXAirHumidty.push([el, ...tempXairHumidity])
         dataXSoilMoisture.push([el, ...tempXSoilMoisture])
         dataXTime.push([`x${index+1}`, ...temTime])
         keyData[el] = `x${index+1}`
      })

      let dataXSoilMoistureResult = [...dataXTime, ...dataXSoilMoisture]
      let dataXAirTempratureResult = [...dataXTime, ...dataXDevice]
      let dataXAirHumidtyResult = [...dataXTime, ...dataXAirHumidty]
      let dataResult = {
         time: keyData,
         air_temprature : dataXAirTempratureResult,
         air_humidity: dataXAirHumidtyResult,
         soil_moisture: dataXSoilMoistureResult,
      }

      return dataResult

      
   } catch (err) {
      console.log(err.message)
   }
}

module.exports = {
   storeSensorFromMqtt,
   getData
}