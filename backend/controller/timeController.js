const model = require('../models/index')
const cronHandler = require('../service/cronHandler')

async function getAll(req, res) {

    try {
        let data = await model.times.findAll()
      
        let message = data.length >= 0 ? "Success" : "data empty" 

        return res.status(200).json({
            status: 'success',
            message,
            count: data.length,
            data,
        })

    } catch(err) {
        return res.json({
            message: err.message,
            count: null,
            data: []

        })
    }
    
}

async function store (req, res) {
    try {
        const {
            jam
        } = req.body
        
        await model.times.create({
           jam
        })

        cronHandler.updateJob()

        return res.status(201).json({
            status: "success",
            message: "Schedule created",
            data: [],
        })
       
    } catch (err) {
        return res.status(500).json({
            status: "failed",
            message: err.message,
            data: [],
        })
    }
}

async function remove (req, res) {
    try {
        const query = req.query

        await model.times.destroy({
            where: {
                id: query.id
            }
        })

        cronHandler.updateJob()

        return res.status(200).json({
            status: "Success",
            message: "Schedule Deleted",
            data: [],
        })

    } catch (err) {
        return res.status(500).json({
            status: "failed",
            message: err.message,
            data: [],
        })
    }
}

async function edit (req, res) {
    try {
        let query = req.query
        let { jam } = req.body

        await model.times.update({
            jam
        },
        { 
            where: {
                id: query.id
            }
        })

        cronHandler.updateJob()

        return res.status(200).json({
            status: "Success",
            message: "Devices Updated",
            data: [],
        })

    } catch (err) {
        return res.status(500).json({
            status: "failed",
            message: err.message,
            data: [],
        })
    }
}


module.exports = {
    getAll,
    store,
    remove,
    edit,
}
