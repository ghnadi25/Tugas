const jwt = require('jsonwebtoken')
const User = require('../models/users')

const auth = rule => {
   return (req, res, next) => {
        try {
            const token = req.header('Authorization')

            if(!token){
                res.status(401).json({
                    status: 'Unauthenticated',
                    message: 'You are not authenticated',
                    data: [],
                })
            }

            jwt.verify(token, process.env.JWT_KEY, (err, decode) => {
    
                if(!err){
                    if(![...rule].find(el => el == decode.rule) ) {
                        res.status(401).json({
                            status: 'Unauthorized',
                            message: 'Unauthorized',
                            data: [],
                        });
                    }
                } else {
                    res.status(401).json({
                        status: 'Unauthenticated',
                        message: 'session expired',
                        data: []
                    })
                } 
            });
    
            next()
        } catch (error) {
            res.status(401).send({
                status: 'Unauthenticated',
                message: error.message,
                data: [],
            })
        }
}


}
module.exports = auth