'use strict';
const model = require('../models/index')
const bcrypt = require('bcrypt')
module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      */
      return model.profile.bulkCreate([{
        nama_lengkap: 'Admin Ganteng',
        no_tlp: '897878786',
        alamat: 'lelea-tugu',
      }], {returning: true}).then(users => {
        return queryInterface.bulkInsert('users', [{
          username: 'munad25',
          email: 'mumunad25@gmail.com',
          password: bcrypt.hashSync('munad2504', 10),
          profile_id: users[0].id,
          rule_id: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        }])
      });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      */
     return queryInterface.bulkDelete('Profile', null, {});
  }
};
