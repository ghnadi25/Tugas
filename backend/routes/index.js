var express = require('express');
var router = express.Router();
var response = require('../service/response');
var hash = require('password-hash');
const model = require('../models/index');
const user = require('../controller/userController');
const devices = require('../controller/devicesController')
const petani = require('../controller/petaniController')
const schedule = require('../controller/scheduleController')
const times = require('../controller/timeController')
const auth = require('../middleware/auth');

const adminAuth = auth(["admin"])
const petaniAuth = auth(["petani"])
const withAuth = auth(["petani", "admin"])

/* GET home page. */
router.get('/', function(req, res, next) {

  res.sendStatus(200);
  const data = [process.env.PORT, 'hello']
  res.json(response.success(data));
});

router.get('/data-sensor',adminAuth, async function(req, res, next) {
  try{
    const dataSensor = await model.DeviceLogs.findAll();

    if(dataSensor.length !== 0){
      res.status(200).json(response.success(dataSensor));
    }else{
      res.status(201).json(response.error('data empty'));
    }

  }
  catch(err){
    res.status(400).json(response.error(err.message));

  }
});

router.get('/devices', adminAuth, devices.getAll)
router.post('/devices', adminAuth, devices.create)
router.delete('/devices', adminAuth, devices.remove)
router.put('/devices', adminAuth, devices.edit)
router.get('/devices/:id/log', adminAuth, devices.getDevicesLogById)

router.get('/profile', adminAuth, petani.getAll)
router.post('/profile', adminAuth, petani.createProfile)
router.delete('/profile', adminAuth, petani.remove)
router.put('/profile', withAuth, petani.updateProfile)
router.get('/profile/:id/users', adminAuth, petani.getUserProfile)

router.get('/userprofile/:id', withAuth, petani.getUserProfile)

router.get('/schedule', adminAuth, schedule.getAll)
router.post('/schedule', adminAuth, schedule.createSchedule)
router.put('/schedule', adminAuth, schedule.update)
router.delete('/schedule', adminAuth, schedule.remove)

router.get('/time-schedule', adminAuth, times.getAll)
router.post('/time-schedule', adminAuth, times.store)
router.put('/time-schedule', adminAuth, times.edit)
router.delete('/time-schedule', adminAuth, times.remove)

router.get('/petani/:id/device', petaniAuth, devices.getDevicesLogById)
router.get('/petani/:id/dashboard', petaniAuth, devices.deviceListByPetaniId)

router.get('/petani/:id/pompa', petaniAuth, devices.getPompaStatus)
router.get('/petani/:id/pompa/:value', petaniAuth, devices.getPompaStatus)
router.put('/petani/water-pump/:id/controll', petaniAuth, devices.waterPumpControll)
router.get('/petani/schedule/:id/:value', petaniAuth, devices.scheduleControll)


router.post('/register', user.create);
router.post('/login', user.login);
module.exports = router;
