const cron = require('cron-job-manager')
const model = require('../models')
const moment = require('moment')
const mqtt = require('mqtt')


cronHandler = new cron()

async function addJob() {
    let dataHours = await model.times.findAll()
    let dataHoursToStr = dataHours.map(item => Number(item.jam.split(":")[0])).toString()
    
    
    cronHandler.add('got-action',`0 0 ${dataHoursToStr} * * *`, async () => {
        let device = await model.devices.findAll({
            where: {
                mode: 2
            }
        })
    
        const measure = await model.schedules.findAll({
            order: [['usia', 'DESC']]
        })
    
        device.forEach(element => {
            let deviceAge = 0
            let measurePick = {}
            try {
                let deviceDate = moment(element.waktu_tanam)
                let curentDate = moment()
                deviceAge = curentDate.diff(deviceDate, 'days') + element.usia_tanam
            } catch (err) {
                console.log(err.message)
            }
    
            if(measure.length > 0){
                measure.forEach(el => {
                    if(deviceAge < el.usia) {
                        measurePick = el
                    } 
                })
            }
    
            if(!measurePick.usia) {
                measurePick = measure.reduce((a,b) => {
                    return (a.usia > b.usia) ? a : b
                })
            }
    
            let waterVolume = ((measurePick.volume_air * 1000) * 165) / 1500
            let fertilizerVolume = ((measurePick.volume_pupuk * 1000) * 165) / 1500
    
            let client = mqtt.connect(process.env.MQTT_HOST)
            client.publish('inTopic', `${element.id};1;${waterVolume};${fertilizerVolume};`)
        });
    }) 

    // cronHandler.start('got-action')
}

async function updateJob () {
    let dataHours = await model.times.findAll()
    let dataHoursToStr = dataHours.map(item => Number(item.jam.split(":")[0])).toString()
    
    cronHandler.update('got-action', `0 0 ${dataHoursToStr} * * *`)
}


function stopJob() {
    cronHandler.stop('got-action')
}




module.exports = {
    addJob,
    stopJob,
    updateJob,
}