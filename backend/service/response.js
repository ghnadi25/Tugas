
module.exports  = {
    success : data => {
       return {
           status : "Ok",
           message : " ",
           data
       }

    },
    error : message => {
        return {
            status : "Error",
            message,
            data : []
        }
    }
}