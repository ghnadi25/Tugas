const mqtt = require('mqtt');
const socketApi = require('../service/socketApi');
const storeData = require('../controller/socketResponse');
const model = require('../models')
const moment = require('moment')



class MqttHandler {
  constructor() {
    this.mqttClient = null;
    this.host = process.env.MQTT_HOST;
    this.username = 'admin'; // mqtt credentials if these are needed to connect
    this.password = 'admin';

  }
  // 1
  connect() {
    // Connect mqtt with credentials (in case of needed, otherwise we can omit 2nd param)
    this.mqttClient = mqtt.connect(this.host, { username: this.username, password: this.password });

    // Mqtt error calback
    this.mqttClient.on('error', (err) => {
      console.log(err);
      this.mqttClient.end();
    });

    // Connection callback
    this.mqttClient.on('connect', () => {
      console.log(`mqtt client connected`);
      console.log(process.env.MQTT_HOST);
    });



    // mqtt subscriptions
    this.mqttClient.subscribe('mqtt-test', {qos: 0});

    // When a message arrives, console.log it
    this.mqttClient.on('message', async  (topic, message) => {
    
      let msg = message.toString();
      if(topic == 'mqtt-test'){
        let splitData = msg.split(';')
       
        let device_id = splitData[0]
        let device_status = splitData[1]
        let soil_moisture_value = splitData[2]
        let air_temprature_value = splitData[3]
        let air_humidity_value= splitData[4]

        // let dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }
        // let currentDate = new Date().toLocaleString('en-US', {hour12: false}).replace(/\//g , "-").replace(/\,/g, "")

        let currentDate = moment().format('M-DD-YYYY hh:mm:ss').toString()
        let checkAvailableDevice = await model.devices.findByPk(device_id)
        if(checkAvailableDevice){
          let data = {
              time: currentDate,
              device_id,
              device_status,
              soil_moisture_value,
              air_temprature_value,
              air_humidity_value
          }

  
          try{

            await storeData.storeSensorFromMqtt(data)
            
            
            let deviceId = await model.devices.findByPk(device_id)
            let petaniDashboard = await storeData.getData({petaniId: deviceId.profile_id})
            socketApi.setEmit(`data-petani-dashboard/${deviceId.profile_id}`, petaniDashboard)
            
            let deviceLogs = await storeData.getData({id: device_id})
            socketApi.setEmit(`data-log/${device_id}`, deviceLogs)

            let testSend = {
              deviceId : device_id
            }



            let dataResponse = {
              status: true,
              message: 'data send success'
            }

            this.sendMessage('server-response', JSON.stringify(dataResponse))



          }catch(err){

            console.log(err)
            let dataResponse = {
              status: false,
              message: err.getMessage,
            }
  
            this.sendMessage('server-response',  JSON.stringify(dataResponse))
          }

        } else {

          let dataResponse = {
            status: false,
            message: 'invalid device id'
          }

          this.sendMessage('server-response', JSON.stringify(dataResponse))


        }

      }
        

        
    });

    this.mqttClient.on('close', () => {
      console.log(`mqtt client disconnected`);
    });
  }

  
  
  // Sends a mqtt message to topic: mytopic
  sendMessage(topic, message) {
    this.mqttClient.publish(String(topic), String(message));
  }

  getMessage(topic){
      this.mqttClient.subscribe(String(topic), {qos : 0});
  }


}

module.exports = MqttHandler;
