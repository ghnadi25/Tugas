let socket_io = require('socket.io');
let io = socket_io();
let socketApi = {};
let dataChart = require('../controller/socketResponse')




io.on('connection', async (socket) => {
    console.log('a user is connected');

    socket.on('data_dahsboard', async (socket) => {
        const data = await dataChart.getData()
        io.sockets.emit('GET_DATA_SENSOR', data)
    })
    
    socket.on('data-logs', async (data) => {
        console.log(data)
    })

    socket.on('init-data', async (data) => {
        let response = await dataChart.getData({id: data.id})
        io.sockets.emit(`init-data/${data.id}`, response)
    })

    socket.on('init-petani-dashboard', async (data) => {
        let response = await dataChart.getData({petaniId: data.id})
        io.sockets.emit(`init-petani-dashboard/${data.id}`, response)
    })

})


socketApi.setEmit = (emit, data) => {
   io.sockets.emit(emit, data);
}


socketApi.io = io;
module.exports = socketApi;