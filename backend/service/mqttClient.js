const mqttHandler = require('./mqttHandler')

let mqttClient = new mqttHandler()
mqttClient.connect()

module.exports = mqttClient