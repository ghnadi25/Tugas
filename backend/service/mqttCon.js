const mqtt = require("mqtt");
const socketApi = require('../service/socketApi');
const storeData = require('../controller/socketResponse');
const model = require('../models')
const moment = require('moment')

function prgMqtt() {
  const options = {
    port: 1883,
    host: process.env.MQTT_HOST,
    clientId: "mqttjs_" + Math.random().toString(16).substr(2, 8),
    username: "admin",
    password: "admin",
    keepalive: 60,
    reconnectPeriod: 1000,
    // protocolId: "MQIsdp"
    // protocolVersion: 3,
    clean: true,
    // encoding: "utf8",
  };


  prgMqtt.client = mqtt.connect(process.env.MQTT_HOST, options);

  prgMqtt.client.on("connect", () => {
    prgMqtt.client.subscribe("mqtt-test");
    console.log("connected MQTT");
  });

  prgMqtt.client.on("message", async (topic, message) => {
    let msg = message.toString();
    if(topic == 'mqtt-test'){
      let splitData = msg.split(';')
     
      let device_id = splitData[0]
      let device_status = splitData[1]
      let soil_moisture_value = splitData[2]
      let air_temprature_value = splitData[3]
      let air_humidity_value= splitData[4]

      // let dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }
      // let currentDate = new Date().toLocaleString('en-US', {hour12: false}).replace(/\//g , "-").replace(/\,/g, "")

      let currentDate = moment().format('M-DD-YYYY HH:mm:ss').toString()
      let checkAvailableDevice = await model.devices.findByPk(device_id)
      if(checkAvailableDevice){
        let data = {
            time: currentDate,
            device_id,
            device_status,
            soil_moisture_value,
            air_temprature_value,
            air_humidity_value
        }


        try{

          await storeData.storeSensorFromMqtt(data)
          
          
          let deviceId = await model.devices.findByPk(device_id)
          let petaniDashboard = await storeData.getData({petaniId: deviceId.profile_id})
          socketApi.setEmit(`data-petani-dashboard/${deviceId.profile_id}`, petaniDashboard)
          
          let deviceLogs = await storeData.getData({id: device_id})
          socketApi.setEmit(`data-log/${device_id}`, deviceLogs)

          let testSend = {
            deviceId : device_id
          }



          let dataResponse = {
            status: true,
            message: 'data send success'
          }




        }catch(err){

          console.log(err)
          let dataResponse = {
            status: false,
            message: err.getMessage,
          }

        }

      } else {

        let dataResponse = {
          status: false,
          message: 'invalid device id'
        }



      }

    }
    // client.end();
  });
}

exports.prgMqtt = prgMqtt;