var express = require('express');
var bodyParser = require('body-parser');
var fromData = require('express-form-data');
const cors = require('cors');



const options = {
    autoClean : true
}


var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
// var mqttClient = require('./service/mqttClient')
// mqttClient.sendMessage('test', 'tos tos')

const { prgMqtt } = require("./service/mqttCon");

prgMqtt();

const cronHandler = require("./service/cronHandler")
cronHandler.addJob()

// client.connect();



var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

app.use(cors());
app.use(fromData.parse(options));
app.use(fromData.format());
app.use(fromData.stream());
app.use(fromData.union());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/users', usersRouter);



module.exports = app;
