'use strict';
module.exports = (sequelize, DataTypes) => {
  const schedule = sequelize.define('schedules', {
    usia: DataTypes.INTEGER,
    volume_air: DataTypes.DOUBLE,
    volume_pupuk: DataTypes.DOUBLE
  }, {});
  schedule.associate = function(models) {
    // associations can be defined here
  };
  return schedule;
};