'use strict';
module.exports = (sequelize, DataTypes) => {
  const pompa = sequelize.define('pompa', {
    device_id: DataTypes.STRING,
    mode: DataTypes.BOOLEAN,
    water_pump: DataTypes.BOOLEAN,
    fertilizer_pump: DataTypes.BOOLEAN
  }, {});
  pompa.associate = function(models) {
    // associations can be defined here
  };
  return pompa;
};