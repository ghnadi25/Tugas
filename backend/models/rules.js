'use strict';
module.exports = (sequelize, DataTypes) => {
  const rules = sequelize.define('rules', {
    rule_name: DataTypes.STRING,
  }, {});
  rules.associate = function(models) {
    rules.hasMany(models.users, {as: 'users', foreignKey:'rule_id'})    
  };
  return rules;
};