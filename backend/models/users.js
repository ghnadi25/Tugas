'use strict';

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {
      defaultScope : {
        attributes : {exclude : ['createdAt', 'updatedAt']}
      },
      scopes:{
      withoutPassword : {exclude : ['password']}
      }
  });
  users.associate = function(models) {
    // associations can be defined here

    users.belongsTo(models.rules, {as : 'rules', foreignKey: 'rule_id'})
    users.belongsTo(models.profile, {as: 'profile', foreignKey: 'profile_id'})
  }
  return users;
};