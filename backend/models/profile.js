'use strict';
module.exports = (sequelize, DataTypes) => {
  const profile = sequelize.define('profile', {
    nama_lengkap: DataTypes.STRING,
    no_tlp: DataTypes.STRING,
    alamat: DataTypes.STRING,
  }, {
    freezeTableName: true,
  });
  profile.associate = function(models) {
    profile.hasMany(models.devices, {foreignKey: 'profile_id', as: 'deviceList'})
    profile.hasOne(models.users, {foreignKey: 'profile_id', as: 'users'})
    // profile.hasOne(models.users, {foreignKey: 'profile_id', as: 'users'})
  };
  return profile;
};