'use strict';
module.exports = (sequelize, DataTypes) => {
  const appConfig = sequelize.define('appConfigs', {
    config_name: DataTypes.STRING,
    params: DataTypes.STRING,
    value: DataTypes.STRING
  }, {});
  appConfig.associate = function(models) {
    // associations can be defined here
  };
  return appConfig;
};