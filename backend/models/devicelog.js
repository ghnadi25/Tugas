
'use strict';
module.exports = (sequelize, DataTypes) => {
  const DeviceLogs= sequelize.define('DeviceLogs', {
    time: DataTypes.STRING,
    air_temprature_value: DataTypes.STRING,
    air_humidity_value: DataTypes.STRING,
    soil_moisture_value: DataTypes.STRING,
    device_status: DataTypes.INTEGER,
  }, {});
  DeviceLogs.associate = function(models) {
    DeviceLogs.belongsTo(models.devices, {
      as: 'devices',
      foreignKey: 'device_id'
    })
  };
  return DeviceLogs;
};