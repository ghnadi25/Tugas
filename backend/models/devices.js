'use strict';
module.exports = (sequelize, DataTypes) => {
  const Devices = sequelize.define('devices', {
    lable: DataTypes.STRING,
    mode: DataTypes.INTEGER,
    usia_tanam: DataTypes.INTEGER,
    waktu_tanam: DataTypes.DATE
  }, {});
  Devices.associate = function(models) {
    Devices.belongsTo(models.profile, {foreignKey: 'profile_id', as: 'profile'})
    Devices.hasMany(models.DeviceLogs, {foreignKey: 'device_id', as: 'deviceLogs'})
  };
  return Devices;
};