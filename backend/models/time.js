'use strict';
module.exports = (sequelize, DataTypes) => {
  const time = sequelize.define('times', {
    jam: DataTypes.STRING
  }, {});
  time.associate = function(models) {
    // associations can be defined here
  };
  return time;
};