

#include <SPI.h>
#include <RH_RF95.h>
#include <SHT1x.h>

#define dataPin  5
#define clockPin 6
SHT1x sht1x(dataPin, clockPin);
// Singleton instance of the radio driver

RH_RF95 rf95;
float frequency = 868.0;

unsigned long startMillis;
unsigned long currentMillis;
//const unsigned long period = 3600000;
const unsigned long period = 10000;

static const uint8_t ID_LENGTH = 38;
struct sensor {
  bool dataStatus;
  char device_id[ID_LENGTH];
  float temprature;
  float humidity;  
};

typedef struct sensor Sensor;
Sensor payload;


struct dataRes {
  int resType;
  char device_id[ID_LENGTH];
  char data[ID_LENGTH];
};

typedef struct dataRes DataRes;
DataRes dataRes;


void initRf95(){
  Serial.println("Start LoRa Client");
  
  if (!rf95.init())
    Serial.println("init failed");

    rf95.setFrequency(frequency);
    rf95.setTxPower(13);
    rf95.setSpreadingFactor(7);
    rf95.setSignalBandwidth(125000);
    rf95.setCodingRate4(5);
    rf95.setSyncWord(0x34);  
  
}

void sendData(){
  Serial.println("Sending to LoRa Server");
  
  rf95.send((uint8_t *)&payload, sizeof(payload));
  rf95.waitPacketSent();
}


void readSensor(){
  float temp_c = sht1x.readTemperatureC();
  float humidity = sht1x.readHumidity();
  Serial.println(temp_c);

  strncpy(payload.device_id, "b1c70a00-d0dd-4261-9cd0-154de961825f", sizeof(payload.device_id) - 1);
  if (temp_c >= 0 && humidity >= 0) {
    
    payload.dataStatus = true;
    payload.temprature = temp_c;
    payload.humidity = humidity;
  } else {
    payload.dataStatus = false;
    payload.temprature = temp_c;
    payload.humidity = humidity;
  }

  
}

void setup() 
{
   initRf95();
  Serial.begin(9600);
  startMillis = millis(); 
}

void loop()
{


//   Send a message to LoRa Server
 currentMillis = millis();
 if(currentMillis - startMillis >= period){

     do {
        readSensor();
        sendData();
        delay(2000);
        Serial.println("data send repetly");
      } while (!rf95.waitAvailableTimeout(2000));


    Serial.println("data send success");
    Serial.println(payload.device_id);
    startMillis = currentMillis;
    
  }

}
