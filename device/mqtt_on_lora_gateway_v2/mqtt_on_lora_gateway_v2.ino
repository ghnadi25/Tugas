

#include <Console.h>
#include <Process.h>
#include <RH_RF95.h>
unsigned long lastMillis = 0;

#define BAUDRATE 115200

RH_RF95 rf95;
float frequency = 868.0;

String mqtt_server = "172.31.9.9";

long lastMsg = 0;
char msg[50];
int value = 0;
String subValue = "";

static const uint8_t ID_LENGTH = 38;
struct sensor {
  bool dataStatus;
  char device_id[ID_LENGTH];
  float temprature = 0.0;
  float humidity = 0.0;
};

typedef struct sensor Sensor;
Sensor payload;

struct dataRes {
  int resType;
  char device_id[ID_LENGTH];
  char data[ID_LENGTH];
};

typedef struct dataRes DataRes;
DataRes dataRes;

void setup() 
{
    // Initialize Bridge
    Bridge.begin(BAUDRATE);
   // Initialize Serial
    Console.begin();

    if (!rf95.init()){
      Console.println("init failed");
    }

    rf95.setFrequency(frequency);
    rf95.setTxPower(13);
    rf95.setSpreadingFactor(7);
    rf95.setSignalBandwidth(125000);
    rf95.setCodingRate4(5);
}

void loop()
{
    if(rf95.available()){
       uint8_t buf[sizeof(payload)];
       uint8_t len = sizeof(payload);
       
       if (rf95.recv(buf, &len)){
          RH_RF95::printBuffer("request: ", buf, len);
           
          memcpy(&payload, &buf, len);
          snprintf (msg, len, buf);
          
          String dataStr = "{\"status\": "+ String(payload.dataStatus) +", \"deviceId\": "+"\""+ String((char *)payload.device_id)+ "\"" +",";
          dataStr += " \"temprature\": "+ String(payload.temprature) +", \"humidity\": "+ String(payload.humidity) +"} ";
        
          
          Console.print("got request & publish: ");
          
//          strncpy(dataRes.device_id, payload.device_id, sizeof(dataRes.device_id) - 1);
//          strncpy(dataRes.data, "Data send success", sizeof(dataRes.data) - 1);
//          dataRes.resType = 1;
          mqtt_publish("mqtt-test", dataStr);
//          rf95.send((uint8_t *)&dataRes, sizeof(dataRes));
//          rf95.waitPacketSent();
          
        
        }
   
   }
}



void mqtt_publish(String topic, String msg)
{
    Process p;    // Create a process and call it "p"
    p.begin("mosquitto_pub"); // Process that launch the "mosquitto_pub" command
    p.addParameter("-d");
    p.addParameter("-h");
    p.addParameter(mqtt_server);
    p.addParameter("-i");
    p.addParameter("deviceId-wY8HTBUTtu");  // Add Baidu Device ID
    p.addParameter("-p");
    p.addParameter("1883");
    p.addParameter("-q");
    p.addParameter("0");
    p.addParameter("-m");
    p.addParameter(msg);// Add data
    p.addParameter("-t");
    p.addParameter(topic);    // Publish to this topic
    p.run();    // Run the process and wait for its termination
  
    // Print arduino logo over the Serial
    // A process output can be read with the stream methods
    while (p.available() > 0) {
      char c = p.read();
      Console.print(c);
    }
  //   Ensure the last bit of data is sent.
    Console.flush();
}
