#include <PubSubClient.h>
#include <BridgeClient.h>
#include <Console.h>
#include <RH_RF95.h>

#define BAUDRATE 115200

RH_RF95 rf95;
float frequency = 868.0;

// Update these with values suitable for your network.

const char* mqtt_server = "172.31.9.9";

BridgeClient net;
PubSubClient client(net);
long lastMsg = 0;
char msg[60];
int value = 0;
String subValue = "";


static const uint8_t ID_LENGTH = 38;
struct sensor {
  bool dataStatus;
  char device_id[ID_LENGTH];
  float temprature = 0.0;
  float humidity = 0.0;
  int moisture = 0;
};

typedef struct sensor Sensor;
Sensor payload;


void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  

  pinMode(A2, OUTPUT);
  digitalWrite(A2, LOW);
  Bridge.begin(BAUDRATE);
  digitalWrite(A2, HIGH);


  randomSeed(micros());

}

void callback(char* topic, byte* payload, unsigned int length) {
  Console.print("Message arrived [");
  Console.print(topic);
  Console.print("] ");
  for (int i = 0; i < length; i++) {
    subValue += (char) payload[i];

  }

  
  uint8_t packetMsg[50];
  subValue.getBytes(packetMsg, 50);
  rf95.send(packetMsg, 50);
  rf95.waitPacketSent();
    Console.print("sent a reply");

    subValue = "";
  
  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(A2, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(A2, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Console.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Console.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Console.print("failed, rc=");
      Console.print(client.state());
      Console.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(A2, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);


  if (!rf95.init()){
    Console.println("init failed");
  }

  rf95.setFrequency(frequency);
  rf95.setTxPower(13);
  rf95.setSpreadingFactor(7);
  rf95.setSignalBandwidth(125000);
  rf95.setCodingRate4(5);

  client.setCallback(callback);
  
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  
  
  if(rf95.available()){
       uint8_t buf[sizeof(payload)];
       uint8_t len = sizeof(payload);
       
       if (rf95.recv(buf, &len)){
          RH_RF95::printBuffer("request: ", buf, len);
           
          memcpy(&payload, &buf, len);
          snprintf (msg, len, buf);
          

          
          
          String dataStr = "{\"status\": "+ String(payload.dataStatus) +", \"deviceId\": "+"\""+ String((char *)payload.device_id)+ "\"" +",";
          
          dataStr += " \"temprature\": "+ String(payload.temprature) +", \"humidity\": "+ String(payload.humidity) +" } ";

          uint8_t dataChr[dataStr.length()];

          dataStr.toCharArray(dataChr, dataStr.length());
          
          client.publish("mqtt-send", dataChr);
          Console.println(rf95.lastRssi(), DEC);
          Console.print("got request & publish: ");
          Console.println((char *)payload.device_id);  
        
        }
   
   }

}
