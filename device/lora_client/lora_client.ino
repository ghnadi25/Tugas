

#include <SPI.h>
#include <RH_RF95.h>
#include <SHT1x.h>

#define dataPin  5
#define clockPin 6

SHT1x sht1x(dataPin, clockPin);
// Singleton instance of the radio driver

RH_RF95 rf95;
float frequency = 868.0;

unsigned long startMillis;
unsigned long currentMillis;
const unsigned long period = 3600000;
int smPin = A2;

int initIterations = 0;

static const uint8_t ID_LENGTH = 38;
struct sensor {
  bool dataStatus;
  char device_id[ID_LENGTH];
  float temprature;
  float humidity;
  int moisture;
};

typedef struct sensor Sensor;
Sensor payload;

void initRf95(){
  Serial.println("Start LoRa Client");
  
  if (!rf95.init())
    Serial.println("init failed");

    rf95.setFrequency(frequency);
    rf95.setTxPower(13);
    rf95.setSpreadingFactor(7);
    rf95.setSignalBandwidth(125000);
    rf95.setCodingRate4(5);
    rf95.setSyncWord(0x34);  
  
}

void sendData(){
  Serial.println("Sending to LoRa Server");
  
  rf95.send((uint8_t *)&payload, sizeof(payload));
}


void readSensor(){
  float temp_c = sht1x.readTemperatureC();
  float humidity = sht1x.readHumidity();
  
  int moisture = analogRead(smPin);
  strncpy(payload.device_id, "8b56e2e5-2037-48ab-bdc9-84b5ead3c946", sizeof(payload.device_id) - 1);
  if (temp_c >= 0 && humidity >= 0) {
    payload.moisture = moisture;
    payload.dataStatus = true;
    payload.temprature = temp_c;
    payload.humidity = humidity;
//    strncpy((char *)payload.device_id, device_id, 32);
  } else {
    payload.dataStatus = false;
    payload.temprature = temp_c;
    payload.humidity = humidity;
//   strncpy((char *)payload.device_id, device_id, 32);
  }

  
}

void setup() 
{
   initRf95();
  Serial.begin(9600);
  startMillis = millis(); 
}

void loop()
{

 if(initIterations <= 20){
    readSensor();
    sendData();
    delay(10000);
    initIterations = initIterations + 1;
 }

//   Send a message to LoRa Server
 currentMillis = millis();
 if(currentMillis - startMillis >= period){
    readSensor();
    sendData();
    Serial.println(payload.device_id);
    startMillis = currentMillis;
    
  }


 
  // Now wait for a reply
//  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
//  uint8_t len = sizeof(buf);
//
//  if (rf95.waitAvailableTimeout(3000))
//  { 
//    // Should be a reply message for us now   
//    if (rf95.recv(buf, &len))
//   {
//      Serial.print("got reply: ");
//      Serial.println((char*)buf);
//      Serial.print("RSSI: ");
//      Serial.println(rf95.lastRssi(), DEC);    
//    }
//    else
//    {
//      Serial.println("recv failed");
//    }
//  }
//  else
//  {
//    Serial.println("No data received");
//  }


}
